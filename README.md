# Stromampel

An application using an AZ-Delivery ESP8266MOD 12-F to display the current electricity mix for Germany in a traffic light kind of way.

The data is updated every 30 minutes (by default) or 5 minutes in case of error. It is provided by the [Fraunhofer institute](https://api.energy-charts.info/). The colors are calculated as [follows](https://energy-charts.info/charts/consumption_advice/chart.htm?l=en&c=DE):
- Green: Share of renewables > average + 10 % or share of renewables > 95%
- Yellow: Average - 10 % < share of renewables < average + 10 %
- Red: Share of renewables < average - 10 %.
Average means the average value of the monthly share of renewable energies of the same month of the last five years.
If a grid congestion is detected, the red light is lit up as well.
The lowest value (red being the lowest, green the highest) of the next two hours is selected.

## Installation

To use the application, you need to run
```bash
cp secret_def.h secret.h
```
and change the default values to values reflecting your 2.4GHz WiFi network.

You need to have the `arduino-cli` installed and configured. Install it for your system, e.g. by running
```bash
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=~/local/bin sudo sh
```
Then install the platform and the JSON library by running
```bash
arduino-cli --additional-urls http://arduino.esp8266.com/stable/package_esp8266com_index.json core install esp8266:esp8266
arduino-cli lib install ArduinoJson
```

If you're on Linux, you might have to give yourself access rights to the port by running
```bash
sudo usermod -a -G dialout $USER
```
After that you need to `reboot` for the changes to take effect.

You can then run the script `run.sh` if the microcontroller is connected to your PC. You might have to adjust the port value in `upload.sh` and `monitor.sh`. For figuring out the port, run
```bash
arduino-cli board list
```

## Setup
The red LED is connected to D5, yellow to D2 and green to D0. Don't forget the appropriate resistors for your LEDs. It might look something like this then:

![Picture of the setup](./StromAmpel.jpg)

Here is a schema:

![Schema of the described setup](./schema.png)

## Status codes

If all LEDs are lit up, something is loading. If they never turn off from the start, the WIFI-connection probably fails.

If one or more LEDs is blinking, an error occurred. The error codes are encoded in binary, with green representing 1, yellow 2 and red 4. The error codes are as follows:
1. HTTPS was unable to connect
2. The HTTP response code was negative, indicating an error
3. The payload of the HTTP response was empty
4. The HTTP response code was neither OK nor MOVED\_PERMANENTLY
5. The time request of the ntp server was unsuccessful
6. The deserialization of the JSON response failed
7. No relevant data was found

/**
 * Main file of the StromAmpel application.
 * Check the README.md for more information.
 */
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <ArduinoJson.h>
#include "secret.h"

/**
 * Defines the pins of the LEDs
 */
#define GREEN D0
#define YELLOW D2
#define RED D5

/**
 * How many elements are to be expected in the API
 * response at most.
 */
#define MAX_ELEMENTS 300

/**
 * Give unknown definitions to the variables defined in
 * secret.h, if the file is not present
 */
#ifndef SSID
#define SSID "<unknown>"
#define PASSWORD "<unknown>"
#define COUNTRY "de"
#define POSTAL_CODE "<unknown>"
#endif

/**
 * The url to make the API requests to
 */
#define URL "https://api.energy-charts.info/signal?country=" COUNTRY "&postal_code=" POSTAL_CODE

/**
 * The Wifi data
 */
const char* ssid = SSID;
const char* password = PASSWORD;

/**
 * How many hours in the future data should be considered
 */
const int hourForecast = 2;

/**
 * How long to wait between each API request
 * (for the error case and for the normal case)
 */
const int errorSeconds = 300;
const int intervalSeconds = 1800;

/**
 * The NTP server URL
 */
const char* ntpServer = "pool.ntp.org";

/**
 * Initialize everything
 */
void setup()
{
    Serial.begin(9600);
    Serial.println();

    pinMode(GREEN, OUTPUT);
    pinMode(YELLOW, OUTPUT);
    pinMode(RED, OUTPUT);

    loading();
    connectWifi();
    configTime(0, 0, ntpServer);
}

/**
 * Stuff that's periodically executed
 */
void loop()
{
    loading();
    String data = requestData(URL);
    int signal = getSignal(data);
    reset();

    if (signal > 2)
    {
        Serial.println("Error!");
        return;
    }

    int colors[] = {RED, YELLOW, GREEN};
    String colorStrings[] = {"Green", "Yellow", "Red"};
    Serial.println(colorStrings[signal]);
    digitalWrite(colors[signal], HIGH);

    Serial.printf("\nWaiting %ds before the next round...",
            intervalSeconds);
    delay(intervalSeconds * 1000);
}

/**
 * Connects to the Wifi network specified by the
 * `ssid` and `password` global constants.
 * Waits until connected (if the specified data is
 * wrong, this means that it waits forever).
 */
void connectWifi()
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    Serial.print("Connecting to WiFi ..");
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print('.');
        delay(1000);
    }
    Serial.println("");
}

/**
 * Ensures there is an active Wifi connection.
 * If there is none, connects to Wifi and waits until
 * connected (using `connectWifi`).
 */
void ensureWifi()
{
    if (WiFi.status() != WL_CONNECTED) {
        Serial.println("Not connected anymore. Reconnecting...");
        connectWifi();
    }
}

/**
 * Creates a secure wifi client and returns it.
 * It is configured so that SSL certificates are ignored.
 */
std::unique_ptr<BearSSL::WiFiClientSecure> getClient()
{
    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
    // Ignore SSL certificate validation
    client->setInsecure();
    return client;
}

/**
 * Initializes an HTTPS client and starts the connection.
 * Returns 0 if successful, 1 otherwise.
 */
int initializeHttpsClient(
    String url, HTTPClient *https)
{
    std::unique_ptr<BearSSL::WiFiClientSecure> client = getClient();

    //Initializing an HTTPS communication using the secure client
    if (!https->begin(*client, url)) {
        Serial.println("[HTTPS] Unable to connect");
        return 1;
    }
    return 0;
}

/**
 * Requests the data from the specified url and returns it as
 * a string. If an error occurs, it calls the `error` function
 * (which waits the time specified in `errorSeconds`) and
 * returns an empty string.
 */
String requestData(String url)
{
    // wait for WiFi connection
    ensureWifi();

    HTTPClient https;
    if (initializeHttpsClient(url, &https))
    {
        error(1);
        return "";
    }

    int httpCode = https.GET();
    if (httpCode < 0) {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
        https.end();
        error(2);
        return "";
    }

    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = https.getString();
        https.end();
        if (payload.length() == 0)
            error(3);
        return payload;
    }

    https.end();
    error(4);
    return "";
}

/**
 * Deserializes the data passed into a `DynamicJsonDocument`.
 * Returns 0 if successful, 1 otherwise.
 */
int deserialize(String data, DynamicJsonDocument *obj)
{
    StaticJsonDocument<64> filter;
    filter["unix_seconds"] = 1;
    filter["signal"] = 1;

    DeserializationError desError = deserializeJson(*obj, data, DeserializationOption::Filter(filter));
    if (desError)
    {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(desError.f_str());
        return 1;
    }
    return 0;
}

/**
 * Gets the time limits with the help of the ntp server.
 * The upper limit is calculated with the `hourForecast`
 * configuration.
 * Returns 0 if successful, 1 otherwise.
 */
int getTimeLimits(long *lowerLimit, long *upperLimit)
{
    *lowerLimit = getTime();
    if (*lowerLimit <= 0)
        return 1;
    *upperLimit = *lowerLimit + hourForecast * 3600;
    return 0;
}

/**
 * Parses the `data` and fills the `JsonArrays` passed with
 * the relevant data for `seconds` and `signals` from
 * the API response.
 * Returns 0 if successful, 1 otherwise.
 */
int getSecondsAndSignals(
    String data, JsonArray *seconds, JsonArray *signals)
{
    DynamicJsonDocument obj(
        JSON_ARRAY_SIZE(MAX_ELEMENTS) * 2 +
        JSON_OBJECT_SIZE(10));

    if (deserialize(data, &obj))
        return 1;

    *seconds = obj["unix_seconds"];
    *signals = obj["signal"];

    return 0;
}

/**
 * Calculates the minimal signal value (with red=0, green=2)
 * in the specified time limit and returns it.
 * If -1 (grid congestion) appeared, this is treated as red (0).
 * Returns 3 if no data matched the limits.
 */
int getMinSignalInLimits(
    JsonArray seconds, JsonArray signals,
    long lowerLimit, long upperLimit)
{
    int min = 3;
    for (int i = 0; i < (int)seconds.size(); i++)
        if ((long)seconds[i] >= lowerLimit &&
                (long)seconds[i] <= upperLimit &&
                (int)signals[i] < min)
            min = signals[i];
    return min < 0 ? 0 : min;
}

/**
 * Parses the passed `data` and returns the minimal signal value
 * (with red=0, yellow=1, green=2).
 * If an error occurred, the `error` function is being called
 * (taking `errorSeconds` of time) and a negative value is returned.
 * Grid congestions are interpreted as red (0).
 */
int getSignal(String data)
{
    if (data.length() == 0)
        // Error got already called
        return -1;

    long lowerLimit;
    long upperLimit;
    if (getTimeLimits(&lowerLimit, &upperLimit))
    {
        error(5);
        return -2;
    }

    JsonArray seconds;
    JsonArray signals;
    if (getSecondsAndSignals(data, &seconds, &signals))
    {
        error(6);
        return -4;
    }

    int min = getMinSignalInLimits(
        seconds, signals, lowerLimit, upperLimit);

    if (min >= 3)
    {
        error(7);
        min = -3;
    }

    Serial.printf("Minimum is %d\n", min);
    return min;
}

/**
 * Puts the LEDs in the *loading* state (all LEDs are lit up).
 */
void loading()
{
    digitalWrite(GREEN, HIGH);
    digitalWrite(YELLOW, HIGH);
    digitalWrite(RED, HIGH);
}

/**
 * Puts the LEDs in the *error* state (the LEDs blink
 * corresponding to the passed `code`, interpreted in binary
 * with green=1, yellow=2, red=4.
 * This function takes `errorSeconds` of time.
 */
void error(int code)
{
    for (int i = 0; i < errorSeconds; i++)
    {
        if (code & 1)
            digitalWrite(GREEN, HIGH);
        if (code & 2)
            digitalWrite(YELLOW, HIGH);
        if (code & 4)
            digitalWrite(RED, HIGH);

        delay(500);
        
        digitalWrite(GREEN, LOW);
        digitalWrite(YELLOW, LOW);
        digitalWrite(RED, LOW);
        delay(500);
    }
}

/**
 * Resets the LEDs (turns them off).
 */
void reset()
{
    digitalWrite(GREEN, LOW);
    digitalWrite(YELLOW, LOW);
    digitalWrite(RED, LOW);
}

/**
 * Gets the current UNIX time from the time server specified
 * in `ntpServer` (configured in `setup`).
 */
unsigned long getTime()
{
    time_t now;
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        Serial.println("Failed to obtain time");
        return(0);
    }
    time(&now);
    return now;
}

